# Orangear Android Maven repository #

* Latest version: 1.6.0
* You can find detailed instructions in your Orangear publisher’s account.

### How do I get set up? ###

1) Open build.gradle file for your project. Add maven repository url: 


```
#!gradle

allprojects {
    repositories {
        jcenter()
        maven {
            url 'https://api.bitbucket.org/1.0/repositories/orangegear/android-maven/raw/master'
         }
    }
}
```


2) Open build.gradle file for your application module, add the following dependency:

```
#!gradle

dependencies {
    …
    compile 'orange:orangesdk:+' //to use the latest available version 
	compile 'orange:orangesdk:1.6.0' //or specify the latest stable version
}
```


3) Add the following code to your activity:


```
#!java

import orangeSDK.Offerwall;

...

Button myButton= (Button) findViewById(R.id.showOfferwall);
        myButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int offerwallColumns=2;
                Offerwall.Show(view.getContext(), "TOKEN",
                        "APP_ID", offerwallColumns);
            }
        });
```

		
Replace "TOKEN" and "APP_ID" with your publisher’s token and app_id in your Publisher’s account (Applications sub-section).

If you are using rewarded Offerwall you can receive information on the user’s total credits and any new credits the user has earned by calling Offerwall.GetRewardedAmount(); at any time in your application.

In addition, you can customize colors by overriding additional parameters in your values.xml file (Optional):  

```
#!xml
<resources>
    //Color customization
    <color name="colorTheme">#ff6600</color>
    <color name="colorOfferBackground">#ffffff</color>
    <color name="colorButtonInstall">#ff6600</color>
    <color name="borderColor">#ff6600</color>
    <color name="ratingStars">#ffbb33</color>
    //Column border width
    <item type="dimen" name="borderWidth">3dip</item>
    //Title text
    <string name="title_rewarded">Install and Open to Get Reward!</string>
    <string name="title_recommended">Offerwall</string>
    //Open links in an embedded webview instead of the browser
    <bool name="open_in_webview">true</bool>
</resources>
```